#!/usr/bin/env python
"""
Publishes the values of the encoders in counts per seconds (CPS).
To convert to rotations per minute (RPM) of the output shaft
RPM = CPS*(60 seconds/minute) / (GEAR RATIO output:input) / (COUNTS Per Revolution)
Example (12 CPR encoder with 120:1 gear ratio, running at 3600 CPS):
3600 CPS * 60 / 120 / 12 = 150RPM (which is the spec'ed speed for our current plastic motors)
"""

import rospy
import me416_utilities as mu
from me416_lab.msg import MotorSpeedsStamped

left_speed = 0
right_speed = 0


def motor_speeds_callback(msg):
    global left_speed
    global right_speed

    left_speed = msg.left
    right_speed = msg.right


def main():
    """Node setup and main ROS loop"""

    #If we are neither on the RPI nor in Gazebo, then just repeat motor_speeds
    is_repeater = not mu.IS_RPI and not mu.IS_GAZEBO

    #Init node. anonymous=True allows multiple launch with automatically assigned names
    rospy.init_node('encoders_publisher', anonymous='True')

    #Prepare publisher on the 'chatter' topic
    pub = rospy.Publisher(
        'motor_speeds_encoders', MotorSpeedsStamped, queue_size=10)

    #Prepare objects for message and encoders
    msg = MotorSpeedsStamped()

    if is_repeater:
        rospy.Subscriber('/motor_speeds', MotorSpeedsStamped,
                         motor_speeds_callback)
        rospy.loginfo(
            'Neither RPi nor Gazebo detected, repeating /motor_speeds')
    else:
        encoder_right = mu.createEncoder('right')
        encoder_left = mu.createEncoder('left')

    #Set rate to use (in Hz)
    rate = rospy.Rate(3)

    while not rospy.is_shutdown():
        msg.header.stamp = rospy.Time.now()
        if is_repeater:
            msg.left = left_speed
            msg.right = right_speed
        else:
            msg.left = encoder_left.get_speed()
            msg.right = encoder_right.get_speed()
        pub.publish(msg)
        rate.sleep()


if __name__ == '__main__':
    try:
        main()
    finally:
        #This is the place to put any "clean up" code that should be executed
        #on shutdown even in case of errors, e.g., closing files or windows
        pass
