#!/usr/bin/env python
"""Utility to test ROSBot camera."""
import time

import cv2


def clock():
    return time.clock_gettime(time.CLOCK_MONOTONIC_RAW)


def capture_n(cap, n=30):
    start_time = clock()
    for count in range(n):
        ret, frame = cap.read()
        print(count + 1, ret, frame.shape, count / (clock() - start_time))
    return frame


cap = cv2.VideoCapture(1)
cap.set(3, 640)
frame = capture_n(cap)
cap.release()

cv2.imwrite('camera_snap.png', frame)
