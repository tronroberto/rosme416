import matplotlib.pyplot as plt
import numpy as np
from math import sin

plt.ion()


def axes():
    """ Prepare standard axes with arrows """
    plt.figure()
    plt.gca().axis('equal')
    plt.xlim((-7, 7))
    plt.ylim((-7, 7))


def plot(f, x1_range=range(-4, 6, 2), x2_range=range(-4, 6, 2)):
    """ Make a 2-D quiver plot of the field f """
    axes()
    plt.quiver([0, -5], [-5, 0], [0, 10], [10, 0],
               angles='xy',
               scale_units='xy',
               scale=1)
    if type(x1_range) == int or len(x1_range) == 1:
        x1_range = np.linspace(-4, 6, x1_range)
    if type(x2_range) == int or len(x2_range) == 1:
        x2_range = np.linspace(-4, 6, x2_range)

    for x1 in x1_range:
        for x2 in x2_range:
            x = np.array([[x1], [x2]])
            fx = f(x)
            plt.quiver(x[0, 0],
                       x[1, 0],
                       fx[0, 0],
                       fx[1, 0],
                       color='blue',
                       angles='xy',
                       scale_units='xy',
                       scale=1)


def to_euler(f):
    """ Create a Euler iteration function from a field  """
    return lambda x, h: x + h * f(x)


def euler_run(euler_step, h, x0, nb_steps, u=None, line_color='red'):
    """ Run Euler step iteration functions, and collect first two coordinates """
    x_all = x0[0:2, :]
    xk = x0
    for step in range(0, nb_steps):
        if u:
            xk = euler_step(xk, u, h)
        else:
            xk = euler_step(xk, h)
        x_all = np.hstack((x_all, xk[0:2, :]))
    plt.plot(x_all[0], x_all[1], color=line_color)


def example_sin():
    f = lambda x: np.array([[1.0], [sin(x[0, 0])]])
    plot(f, np.linspace(-5, 5, 11), np.linspace(-5, 5, 11))
