#!/usr/bin/env python
'''
Simple node that repeats images from a compressed topic to a non-compressed one.
'''

import rospy
import numpy as np
from cv_bridge import CvBridge
from sensor_msgs.msg import Image
import cv2


class ImageRepeater(object):
    def __init__(self):
        rospy.Subscriber(
            '/raspicam_node/image',
            Image,
            callback=self.callback,
            queue_size=1,
            buff_size=2**18
        )  #the buff_size=2**18 avoids delays due to the queue buffer being too small for images
        self.pub = rospy.Publisher('image_repeated', Image, queue_size=1)
        self.bridge = CvBridge()

    def callback(self, msg):
        img = self.bridge.imgmsg_to_cv2(msg, 'bgr8')
        img = cv2.flip(img, -1)
        
        msg_repeated=self.bridge.cv2_to_imgmsg(img, 'bgr8')
        self.pub.publish(msg_repeated)


if __name__ == '__main__':
    rospy.init_node('image_repeater')
    ir = ImageRepeater()
    rospy.spin()
